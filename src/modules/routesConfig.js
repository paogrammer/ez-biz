const routesConfig = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    messageId: 'sidebar.one',
    type: 'item',
    url: '/dashboards/e-commerce',
  },
  {
    id: 'inventory',
    title: 'Inventory',
    messageId: 'sidebar.two',
    type: 'item',
    url: '/third-party/react-table',
  },
  {
    id: 'helpdesk',
    title: 'Helpdesk',
    messageId: 'sidebar.three',
    type: 'item',
    url: '/extra-pages/knowledge-base',
  },
];
export default routesConfig;
